#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

double calcula_desvio_medio(vector<double> vec_valores){
	
	double somatorio = 0, somatorio1 = 0;
	double instante1 = 0, instante2 = 0, valormedio = 0;
	for (int j = 0; j < vec_valores.size(); j++){
		somatorio1 = somatorio1 + (vec_valores[j]);
		
	} 
	
	valormedio = ((somatorio1)/(vec_valores.size()));
	cout<<"O valor medio e = "<<valormedio<<endl;
	
	for (int i = 0; i < vec_valores.size(); i++){
		somatorio = somatorio + pow((vec_valores[i] - valormedio), 2);
		
	}
	
	instante1 = ((somatorio)/(vec_valores.size()));
	instante2 = sqrt(instante1);
	return instante2;
}



int main(){
	
	vector<double> vec_valores;
	double desviomedio;
	int numero_pontos;
	double aux;
	
	cout<<"Insira o numero de pontos:"<<endl;
	cin>>numero_pontos;
	
	vec_valores.reserve(numero_pontos);

	
	cout<<"Insira os valores de cada medida:"<<endl;
	
	for(int i=0; i < numero_pontos; i++){
		cin>>aux;
		vec_valores.push_back(aux);
		aux = 0;
	}
	desviomedio = calcula_desvio_medio(vec_valores);
	cout<<"O desvio medio e = "<<desviomedio<<endl;
}
